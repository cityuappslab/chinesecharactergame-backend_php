@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="col-sm-offset-1 col-sm-10">
			<div class="panel panel-default">
				<div class="panel-heading">
					New Character Set
				</div>

				<div class="panel-body">
					<!-- Display Validation Errors -->
					@include('common.errors')

					<!-- New characterSet Form -->
					<form action="/manageCharacterSet/character-set" method="POST" class="form-horizontal">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<!-- characterSet Name -->
						<div class="form-group">
							<label for="characterSet-characterSetName" class="col-sm-3 control-label">Character Set Name</label>

							<div class="col-sm-6">
								<input type="text" name="characterSetName" id="characterSet-characterSetName" class="form-control">
							</div>
						</div>

						{{--<div class="form-group">--}}
							{{--<label for="characterSet-numOfCharacter" class="col-sm-3 control-label">Number of character</label>--}}

							{{--<div class="col-sm-6">--}}
								{{--<input type="text" name="numOfCharacter" id="characterSet-description" class="form-control">--}}
							{{--</div>--}}
						{{--</div>--}}

						<!-- Add characterSet Button -->
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-6">
								<button type="submit" class="btn btn-default">
									<i class="fa fa-btn fa-plus"></i>Add characterSet
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>

			<!-- Current characterSets -->
			@if (count($characterSets) > 0)
				<div class="panel panel-default">
					<div class="panel-heading">
						Current characterSet
					</div>

					<div class="panel-body">
						<table class="table table-striped characterSet-table">
							<thead>
								<th>characterSet</th>
								<th>&nbsp;</th>
							</thead>
							<tbody>
								@foreach ($characterSets as $characterSet)
									<tr>
										<td class="table-text"><div>{{ $characterSet->characterSetName }}</div></td>

										<!-- characterSet Delete Button -->
										<td>
											<form action="{{url('manageCharacter/character')}}" method="GET">
												<input type="hidden" name="characterSet_id" value="{{$characterSet->id }}">
												{{--<input type="hidden" name="_token" value="{{ csrf_token() }}">--}}
												<button type="submit" id="{{ $characterSet->id }}" class="btn btn-danger">
													<i class="fa fa-btn fa-edit"></i>Edit
												</button>
											</form>
										</td>
										<td>
											<form action="{{url('manageCharacterSet/delete-character-set/')}}" method="POST">
												<input type="hidden" name="characterSet_id" value="{{$characterSet->id }}">
												{{--<input type="hidden" name="_token" value="{{ csrf_token() }}">--}}
												<button type="submit" id="{{$characterSet->id }}" class="btn btn-danger">
													<i class="fa fa-btn fa-trash"></i>Delete
												</button>
											</form>
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			@endif
		</div>
	</div>
@endsection
