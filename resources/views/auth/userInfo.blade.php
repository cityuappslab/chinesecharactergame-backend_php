@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Register</div>
                    <div class="panel-body">
                        {!! Form::model($user,['method'=>'POST','url'=>['userinfo/update'] , 'files' => true],['class'=>'form-horizontal']) !!}

                        <div class="form-group">
                            {!! Form::label('name','Name: ',['class'=>'col-md-4 control-label']) !!}
                            {!! Form::text('name',null,['class'=>'form-control']) !!}
                        </div>


                        <div class="form-group">
                            {!! Form::label('self_into','Self Introduction: ',['class'=>'col-md-4 control-label']) !!}
                            {!! Form::text('self_into',null,['class'=>'form-control ']) !!}
                        </div>



                        <div class="form-group">
                            {!! Form::label('','Person Picture: ',['class'=>'col-md-4 control-label']) !!}
                            {!! Form::file('profile_photo', null) !!}
                            @if(Auth::user()->getProfileImage()!=false)
                                <img src={{Auth::user()->getMediumProfileImage()}} />
                            @endif
                        </div>

                        <div class="form-group">
                            <div class="col-md-5 col-md-offset-4 text-center alert alert-success">
                                <strong>Updated at {{$user->getLastUpdateTimeForHumans()}}</strong>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i> Update Profile
                                </button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    @if(isset($result))
                        <div class="alert alert-success">
                            <strong>{{$result}}</strong>
                        </div>
                    @endif
                    @include('common.errors')
                </div>
            </div>
        </div>
    </div>
@endsection
