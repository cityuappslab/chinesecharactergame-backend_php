@extends('layouts.app')

@section('content')

    <div class="col-sm-offset-1 col-sm-10">
        <div class="panel panel-default">
            <div class="panel-heading ">
                Record Dashboard
            </div>

            <div class="panel-body">
                <div class="card">

                    <div class="card-content btn-primary">
                        <table class="table-condensed table-inverse">
                            <tbody>
                            <tr class="info">
                                <th>Name:</th>
                                <td>{{$user['name']}}&nbsp</td>
                            </tr>
                            <tr class="info">
                                <th>Email:</th>
                                <td>{{$user['email']}}&nbsp</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    </div>

                <div class="card">

                    <div class="card-content btn-primary">
                        <table class="table-condensed table-inverse">
                            <tbody>
                            <tr class="info">
                                <th>Name:</th>
                                <td>{{$game['gameName']}}&nbsp</td>
                            </tr>
                            <tr class="info">
                                <th>Description:</th>
                                <td>{{$game['description']}}&nbsp</td>
                            </tr>
                            <tr class="info">
                                <th>Game Level:</th>
                                <td>{{$gamelevel['level']}}&nbsp</td>
                            </tr>
                            <tr class="info">
                                <th>Duration: (s)</th>
                                <td>{{$gamelevel['duration']}}&nbsp</td>
                            </tr>

                            @if($game['id'] == 11)

                            <tr class="info">
                                <th>Text Disappear Time: (s)</th>
                                <td>{{$gamelevel['text_disappear_time']}}&nbsp</td>
                            </tr>

                            <tr class="info">
                                <th>Decrease Percentage: (%)</th>
                                <td>{{$gamelevel['decrease_percentage']}}&nbsp</td>
                            </tr>
                            
                            @endif

                            <tr class="info">
                                <th>NumOfRow:</th>
                                <td>{{$gamelevel['numOfRow']}}&nbsp</td>
                            </tr>
                            <tr class="info">
                                <th>NumOfCol:</th>
                                <td>{{$gamelevel['numOfCol']}}&nbsp</td>
                            </tr>
                            {{--<tr class="info">--}}
                                {{--<th>Character Set Name:</th>--}}
                                {{--<td>{{$characterSetName}}&nbsp</td>--}}
                            {{--</tr>--}}
                            </tbody>
                        </table>
                    </div>
                </div>


                <div class="panel-body">


                <br/><br/>

                <!-- Card Projects -->
                    <div class="col-md-12 col-md-offset-0">
                        <table class="table table-condensed table-inverse" style="width:100%">
                            <tr>
                                <th>No.trial</th>
                                <th>Response Time (ms)</th>
                                <th>numOfRow</th>
                                <th>numOfCol</th>
                                <th>Char</th>
                                <th>Result</th>
                                <th>First Trial</th>
                                <th>Answer Char</th>
                                <th>[1][1]</th>
                                <th>[1][2]</th>
                                <th>[1][3]</th>
                                <th>[1][4]</th>
                                <th>[1][5]</th>
                                <th>[2][1]</th>
                                <th>[2][2]</th>
                                <th>[2][3]</th>
                                <th>[2][4]</th>
                                <th>[2][5]</th>
                                <th>[3][1]</th>
                                <th>[3][2]</th>
                                <th>[3][3]</th>
                                <th>[3][4]</th>
                                <th>[3][5]</th>
                                <th>[4][1]</th>
                                <th>[4][2]</th>
                                <th>[4][3]</th>
                                <th>[4][4]</th>
                                <th>[4][5]</th>

                            </tr>
                            @foreach($trials as $trial)
                                <tr>
                                    <td>{{$trial->trial}}&nbsp</td>
                                    <td>{{$trial->responseTime}}&nbsp</td>
                                    <td>{{$trial->numOfRow}}&nbsp</td>
                                    <td>{{$trial->numOfCol}}&nbsp</td>
                                    <td>{{$trial->char}}&nbsp</td>
                                    <td>{{$trial->outcome}}&nbsp</td>
                                    @if ($trial->firsttime == 0)
                                    <td>{{"False"}}&nbsp</td>
                                    @elseif ($trial->firsttime == 1)
                                    <td>{{"True"}}&nbsp</td>
                                    @endif
                                    <td>{{$trial->ans}}&nbsp</td>
                                    <td>{{$trial->row1col1}}&nbsp</td>
                                    <td>{{$trial->row1col2}}&nbsp</td>
                                    <td>{{$trial->row1col3}}&nbsp</td>
                                    <td>{{$trial->row1col4}}&nbsp</td>
                                    <td>{{$trial->row1col5}}&nbsp</td>
                                    <td>{{$trial->row2col1}}&nbsp</td>
                                    <td>{{$trial->row2col2}}&nbsp</td>
                                    <td>{{$trial->row2col3}}&nbsp</td>
                                    <td>{{$trial->row2col4}}&nbsp</td>
                                    <td>{{$trial->row2col5}}&nbsp</td>
                                    <td>{{$trial->row3col1}}&nbsp</td>
                                    <td>{{$trial->row3col2}}&nbsp</td>
                                    <td>{{$trial->row3col3}}&nbsp</td>
                                    <td>{{$trial->row3col4}}&nbsp</td>
                                    <td>{{$trial->row3col5}}&nbsp</td>
                                    <td>{{$trial->row4col1}}&nbsp</td>
                                    <td>{{$trial->row4col2}}&nbsp</td>
                                    <td>{{$trial->row4col3}}&nbsp</td>
                                    <td>{{$trial->row4col4}}&nbsp</td>
                                    <td>{{$trial->row4col5}}&nbsp</td>
                                </tr>
                                @endforeach
                                </tbody>
                        </table>

                        <div class="col-md-10 col-md-offset-9">
                        <a href="{{url('/exportgamerecord/'.$trial->gamerecord_id)}}">
                            <button type="submit" class="btn btn-primary">
                                <i class="fa fa-btn fa-save"></i>Export Data by Game Record
                            </button>
                        </a>
                        </div>
                        

                    </div>

            </div>      

        </div>
    </div>
    <br/><br/><br/>


    </div>

    <style>
        .card {
            box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
        }

        .card {
            margin-top: 10px;
            box-sizing: border-box;
            border-radius: 2px;
            background-clip: padding-box;
        }

        .card span.card-title {
            color: #fff;
            font-size: 24px;
            font-weight: 300;
            text-transform: uppercase;
        }

        .card .card-image {
            position: relative;
            overflow: hidden;
        }

        .card .card-image img {
            border-radius: 2px 2px 0 0;
            background-clip: padding-box;
            position: relative;
            z-index: -1;
        }

        .card .card-image span.card-title {
            position: absolute;
            bottom: 0;
            left: 0;
            padding: 16px;
        }

        .card .card-content {
            padding: 16px;
            border-radius: 0 0 2px 2px;
            background-clip: padding-box;
            box-sizing: border-box;
        }

        .card .card-content p {
            margin: 0;
            color: inherit;
        }

        .card .card-content span.card-title {
            line-height: 48px;
        }

        .card .card-action {
            border-top: 1px solid rgba(160, 160, 160, 0.2);
            padding: 16px;
        }

        .card .card-action a {
            color: #ffab40;
            margin-right: 16px;
            transition: color 0.3s ease;
            text-transform: uppercase;
        }

        .card .card-action a:hover {
            color: #ffd8a6;
            text-decoration: none;
        }
    </style>


@endsection

