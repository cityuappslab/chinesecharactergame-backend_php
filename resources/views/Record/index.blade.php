@extends('layouts.app')

@section('content')
    <div class="col-sm-offset-1 col-sm-10">
        <div class="panel panel-default">
            <div class="panel-heading ">
                Record Dashboard
            </div>

            <div class="panel-body">
                <a href="{{url('/records/users')}}">
                    <button type="button" class="btn btn-primary btn-lg btn-block">User Record</button>
                </a>
                <br>
                <div class="dropdown">
                    <button class="btn btn-primary btn-primary btn-lg btn-block dropdown-toggle" type="button" data-toggle="dropdown">Game Record
                        <span class="caret"></span></button>
                    <ul class="dropdown-menu btn-lg btn-block">
                        @foreach($games as $game)
                            <li><a href="{{$game['url']}}">{{$game['gameName']}}</a></li>
                        @endforeach
                    </ul>
                </div>


                <br>
            </div>
        </div>


    </div>
    <div class="container">
    </div>
@endsection

