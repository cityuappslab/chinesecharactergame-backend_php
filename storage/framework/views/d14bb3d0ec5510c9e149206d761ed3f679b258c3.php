<?php $__env->startSection('content'); ?>
    <div class="container">

        <!-- Display Validation Errors -->
        <?php if($completed==false): ?>
            <div class="alert alert-danger">
                <strong>Some information of characters is not completed such as character,picture,audio</strong>
            </div>
        <?php endif; ?>
        <?php echo $__env->make('common.errors', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <div class="col-sm-offset-1 col-sm-10">

            <div class="panel panel-default">
                <div class="panel-heading">
                    New Character Set
                </div>

                <div class="panel-body">

                <!-- New characterSet Form -->
                    <form action="<?php echo e(url('manageCharacter/new-character')); ?>" method="POST" class="form-horizontal"
                          enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">

                        <input type="hidden" name="characterSet_id" value="<?php echo e($characterSet_id); ?>">
                        <!-- character Name -->
                        <div class="form-group">
                            <label for="character-name" class="col-sm-3 control-label">Character</label>

                            <div class="col-sm-6">
                                <input type="text" name="oneCharacter" id="oneCharacter" class="form-control"
                                       value="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="character-img" class="col-sm-3 control-label">Character Image</label>

                            <div class="col-sm-6">
                                <label for="exampleInputFile">File input</label>
                                <input type="file" id="exampleInputFile" class="form-control" name='image'>
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="character-img" class="col-sm-3 control-label">Character Soundtrack</label>

                            <div class="col-sm-6">
                                <label for="exampleInputFile">File input</label>
                                <input type="file" id="exampleInputFile" class="form-control" name='audio'>
                            </div>
                        </div>


                    <!-- Add character Button -->
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" class="btn btn-default">
                                    <i class="fa fa-btn fa-save"></i>Save
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>




            <?php foreach($characters as $character): ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <?php echo e('No '.$index++.' Character : '.$character->oneCharacter); ?>

                    </div>

                    <div class="panel-body">
                        <!-- Display Validation Errors -->
                        <!-- New Character Form -->
                        <form action="<?php echo e(url('manageCharacter/character')); ?>" method="POST" class="form-horizontal"
                              enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                            <input type="hidden" name="character_id" value="<?php echo e($character->id); ?>">
                            <!-- character Name -->
                            <div class="form-group">
                                <label for="character-name" class="col-sm-3 control-label">Character</label>

                                <div class="col-sm-6">
                                    <input type="text" name="oneCharacter" id="oneCharacter" class="form-control"
                                           value="<?php echo e($character->oneCharacter); ?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="character-img" class="col-sm-3 control-label">Character Image</label>

                                <div class="col-sm-6">
                                    <label for="exampleInputFile">File input</label>
                                    <input type="file" id="exampleInputFile" class="form-control" name='image'>
                                </div>
                            </div>

                            <?php if($character->image_url!='null'): ?>
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-6">
                                        <img src="<?php echo e(url($character->image_url)); ?>">
                                    </div>
                                </div>
                            <?php endif; ?>

                            <div class="form-group">
                                <label for="character-img" class="col-sm-3 control-label">Character Soundtrack</label>

                                <div class="col-sm-6">
                                    <label for="exampleInputFile">File input</label>
                                    <input type="file" id="exampleInputFile" class="form-control" name='audio'>
                                </div>
                            </div>

                            <?php if($character->audio_url!='null'): ?>
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-6">
                                        <audio controls>
                                            <source src="<?php echo e(url($character->audio_url)); ?>" type="audio/mpeg">
                                            Your browser does not support the audio element.
                                        </audio>
                                    </div>
                                </div>
                        <?php endif; ?>

                        <!-- Add character Button -->
                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-6">
                                    <button type="submit" class="btn btn-default">
                                        <i class="fa fa-btn fa-save"></i>Save
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            <?php endforeach; ?>

        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>