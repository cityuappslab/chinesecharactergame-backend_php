<?php $__env->startSection('content'); ?>

    <div class="container">


        <div class="col-sm-offset-1 col-sm-12">

            <?php echo $__env->make('common.errors', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

            <?php if($game->isCompleted()==false): ?>
                <div class="alert alert-danger">
                    <strong>Some information of game is not completed. Please select the character set and save</strong>
                </div>
            <?php endif; ?>

            <?php for($i=0;$i<4;$i++): ?>
                <?php /*<?php echo e(dd($gamelevels[$i*5])); ?>*/ ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <?php echo e($game->gameName); ?> - Level <?php echo e(($range[0]+$i*5).'-'.(($range[0]+$i*5)+4)); ?>

                    </div>

                    <div class="panel-body">
                        <!-- Display Validation Errors -->

                        <!-- Edit Game Level Form -->
                        <form action="<?php echo e(url('manageGameLevel/game-level-detail')); ?>" method="POST"
                              class="form-horizontal">
                            <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                            <input type="hidden" name="game_id" value="<?php echo e($game->id); ?>">
                            <input type="hidden" name="block" value="<?php echo e($block); ?>">
                            <input type="hidden" name="index" value="<?php echo e($i); ?>">
                            <input type="hidden" name="level" value="<?php echo e($range[0]+$i*5); ?>">


                            <div class="form-group">
                                <label for="game-description" class="col-sm-3 control-label">
                                    Number of Col
                                </label>

                                <div class="col-sm-8">

                                    <select name="numOfCol" class="form-control">

                                        <?php foreach($numOfCols as $numOfCol): ?>

                                            <?php if($gamelevels[$i*5]['numOfCol']==$numOfCol): ?>
                                                <option value=<?php echo e($numOfCol); ?> selected><?php echo e($numOfCol); ?></option>
                                            <?php else: ?>
                                                <option value=<?php echo e($numOfCol); ?>><?php echo e($numOfCol); ?></option>
                                            <?php endif; ?>
                                        <?php endforeach; ?>

                                    </select>

                                </div>
                            </div>

                            <div class="form-group">
                                <label for="game-description" class="col-sm-3 control-label">
                                    Number of Row
                                </label>

                                <div class="col-sm-8">

                                    <select name="numOfRow" class="form-control">

                                        <?php foreach($numOfRows as $numOfRow): ?>

                                            <?php if($gamelevels[$i*5]['numOfRow']==$numOfRow): ?>
                                                <option value=<?php echo e($numOfRow); ?>

                                                        selected><?php echo e($numOfRow); ?></option>
                                            <?php else: ?>
                                                <option value=<?php echo e($numOfRow); ?>><?php echo e($numOfRow); ?></option>
                                            <?php endif; ?>
                                        <?php endforeach; ?>

                                    </select>

                                </div>
                            </div>


                            <!-- Edit Game Level Button -->
                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-6">
                                    <button type="submit" class="btn btn-default">
                                        <i class="fa fa-btn fa-save"></i>Save
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            <?php endfor; ?>

        </div>


    </div>




<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>