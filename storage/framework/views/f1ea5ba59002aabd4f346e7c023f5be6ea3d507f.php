<?php $__env->startSection('content'); ?>
    <div class="col-sm-offset-1 col-sm-10">
        <div class="panel panel-default">
            <div class="panel-heading ">
                Record Dashboard
            </div>

            <div class="panel-body">
                <a href="<?php echo e(url('/records/users')); ?>">
                    <button type="button" class="btn btn-primary btn-lg btn-block">User Record</button>
                </a>
                <br>
                <div class="dropdown">
                    <button class="btn btn-primary btn-primary btn-lg btn-block dropdown-toggle" type="button" data-toggle="dropdown">Game Record
                        <span class="caret"></span></button>
                    <ul class="dropdown-menu btn-lg btn-block">
                        <?php foreach($games as $game): ?>
                            <li><a href="<?php echo e($game['url']); ?>"><?php echo e($game['gameName']); ?></a></li>
                        <?php endforeach; ?>
                    </ul>
                </div>


                <br>
            </div>
        </div>


    </div>
    <div class="container">
    </div>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>