<?php $__env->startSection('content'); ?>
    <div class="col-sm-offset-2 col-sm-8">
        <div class="panel panel-default">
            <div class="panel-heading ">
                Dashboard
            </div>

            <div class="panel-body">
                <a href="<?php echo e(url('manageGame/game')); ?>">
                    <button type="button" class="btn btn-primary btn-lg btn-block">Manage Game</button>
                </a>
                <br>
                <a href="<?php echo e(url('manageCharacterSet/character-set')); ?>">
                    <button type="button" class="btn btn-primary btn-lg btn-block">Manage Character Set</button>
                </a>
                <br>
                <a href="<?php echo e(url('records')); ?>">
                    <button type="button" class="btn btn-primary btn-lg btn-block">Read Statistics</button>
                </a>
                <br>


                <br>
            </div>
        </div>


    </div>
    <div class="container">
    </div>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>