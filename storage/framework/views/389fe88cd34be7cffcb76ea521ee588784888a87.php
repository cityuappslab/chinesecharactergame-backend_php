<?php $__env->startSection('content'); ?>
    <div class="col-sm-offset-1 col-sm-10">
        <div class="panel panel-default">
            <div class="panel-heading ">
                User Game Records
            </div>

            <div class="panel-body">
                <div class="col-md-10 col-md-offset-1">
                    <div class="card">

                        <div class="card-content btn-primary">
                            

                        <div class="card-action bg-info">


                            
                        </div>
                    </div>
                </div>


                <br>
            </div>
        </div>


    </div>
    <div class="container">
    </div>



    <style>
        .card {
            box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
        }

        .card {
            margin-top: 10px;
            box-sizing: border-box;
            border-radius: 2px;
            background-clip: padding-box;
        }

        .card span.card-title {
            color: #fff;
            font-size: 24px;
            font-weight: 300;
            text-transform: uppercase;
        }

        .card .card-image {
            position: relative;
            overflow: hidden;
        }

        .card .card-image img {
            border-radius: 2px 2px 0 0;
            background-clip: padding-box;
            position: relative;
            z-index: -1;
        }

        .card .card-image span.card-title {
            position: absolute;
            bottom: 0;
            left: 0;
            padding: 16px;
        }

        .card .card-content {
            padding: 16px;
            border-radius: 0 0 2px 2px;
            background-clip: padding-box;
            box-sizing: border-box;
        }

        .card .card-content p {
            margin: 0;
            color: inherit;
        }

        .card .card-content span.card-title {
            line-height: 48px;
        }

        .card .card-action {
            border-top: 1px solid rgba(160, 160, 160, 0.2);
            padding: 16px;
        }

        .card .card-action a {
            color: #ffab40;
            margin-right: 16px;
            transition: color 0.3s ease;
            text-transform: uppercase;
        }

        .card .card-action a:hover {
            color: #ffd8a6;
            text-decoration: none;
        }

        hr {
            -moz-border-bottom-colors: none;
            -moz-border-image: none;
            -moz-border-left-colors: none;
            -moz-border-right-colors: none;
            -moz-border-top-colors: none;
            border-color: #555555;
            border-style: solid none;
            border-width: 1px 0;
            margin: 18px 0;
        }
    </style>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>