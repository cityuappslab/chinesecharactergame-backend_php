<?php $__env->startSection('content'); ?>

    <div class="container">


        <div class="col-sm-offset-1 col-sm-12">

            <?php echo $__env->make('common.errors', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

            <?php if($game->isCompleted()==false): ?>
                <div class="alert alert-danger">
                    <strong>Some information of game is not completed. Please select the character set and save</strong>
                </div>
            <?php endif; ?>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo e($game->gameName); ?> - Level 1-20
                </div>

                <div class="panel-body">
                    <!-- Display Validation Errors -->

                    <!-- Edit Game Level Form -->
                    <form action="<?php echo e(url('manageGameLevel/game-level')); ?>" method="POST" class="form-horizontal">
                        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                        <input type="hidden" name="game_id" value="<?php echo e($game->id); ?>">
                        <input type="hidden" name="level" value="1">
                        <!-- Inital response time -->
                        <div class="form-group">
                            <label for="game-startTime" class="col-sm-3 control-label">Start of response time</label>

                            <div class="col-sm-8">
                                <input type="text" name="startTime" id="game-startTime" class="form-control"
                                       value="<?php echo e($gamelevel[1]->duration); ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="game-decreaseTime" class="col-sm-3 control-label">Decreasing time per
                                level</label>

                            <div class="col-sm-8">
                                <input type="text" name="decreaseTime" id="game-decreaseTime" class="form-control"
                                       value="<?php echo e($difference[1]); ?>">
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="game-description" class="col-sm-3 control-label">
                                Character Set
                            </label>

                            <div class="col-sm-8">
                                <?php if($characterSets->isEmpty()): ?>
                                    <div class="alert alert-danger">
                                        Does exist any character set
                                    </div>
                                <?php else: ?>
                                    <select name="charaterset_id" class="form-control">

                                        <?php foreach($characterSets as $characterSet): ?>
                                            <?php if($characterSet->isCompleted()==true): ?>
                                                <?php if($gamelevel[1]->characterSet_id==$characterSet->id): ?>
                                                    <option value="<?php echo e($characterSet->id); ?>"
                                                            selected><?php echo e($characterSet->characterSetName); ?></option>
                                                <?php else: ?>
                                                    <option value="<?php echo e($characterSet->id); ?>"><?php echo e($characterSet->characterSetName); ?></option>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        <?php endforeach; ?>

                                    </select>
                                <?php endif; ?>
                            </div>
                        </div>




                        <!-- Edit Game Level Button -->
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" class="btn btn-default">
                                    <i class="fa fa-btn fa-save"></i>Save
                                </button>
                            </div>
                        </div>
                    </form>

                    <div class="form-group">
                        <label for="game-description" class="col-sm-3 control-label">
                            Matrix Size edit per 5 level
                        </label>

                        <div class="col-sm-8">
                            <form action="<?php echo e(url('manageGameLevel/game-level-detail')); ?>" method="GET">
                                <input type="hidden" name="game_id" value="<?php echo e($game->id); ?>">
                                <input type="hidden" name="block" value="1">
                                <?php /*<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">*/ ?>
                                <button type="submit" id="<?php echo e($game->id); ?>" class="btn btn-danger">
                                    <i class="fa fa-btn fa-edit"></i>Edit
                                </button>
                            </form>
                        </div>
                    </div>

                </div>
            </div>

        </div>


        <div class="col-sm-offset-1 col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo e($game->gameName); ?> - Level 21-40
                </div>

                <div class="panel-body">
                    <!-- Display Validation Errors -->

                    <!-- Edit Game Level Form -->
                    <form action="<?php echo e(url('manageGameLevel/game-level')); ?>" method="POST" class="form-horizontal">
                        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                        <input type="hidden" name="game_id" value="<?php echo e($game->id); ?>">
                        <input type="hidden" name="level" value="21">
                        <!-- Inital response time -->
                        <div class="form-group">
                            <label for="game-startTime" class="col-sm-3 control-label">Start of response time</label>

                            <div class="col-sm-8">
                                <input type="text" name="startTime" id="game-startTime" class="form-control"
                                       value="<?php echo e($gamelevel[21]->duration); ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="game-decreaseTime" class="col-sm-3 control-label">Decreasing time per
                                level</label>

                            <div class="col-sm-8">
                                <input type="text" name="decreaseTime" id="game-decreaseTime" class="form-control"
                                       value="<?php echo e($difference[21]); ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="game-description" class="col-sm-3 control-label">
                                Character Set
                            </label>

                            <div class="col-sm-8">
                                <?php if($characterSets->isEmpty()): ?>
                                    <div class="alert alert-danger">
                                        Does exist any character set
                                    </div>
                                <?php else: ?>
                                    <select name="charaterset_id" class="form-control">

                                        <?php foreach($characterSets as $characterSet): ?>
                                            <?php if($characterSet->isCompleted()==true): ?>
                                                <?php if($gamelevel[21]->characterSet_id==$characterSet->id): ?>
                                                    <option value="<?php echo e($characterSet->id); ?>"
                                                            selected><?php echo e($characterSet->characterSetName); ?></option>
                                                <?php else: ?>
                                                    <option value="<?php echo e($characterSet->id); ?>"><?php echo e($characterSet->characterSetName); ?></option>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        <?php endforeach; ?>

                                    </select>
                                <?php endif; ?>
                            </div>
                        </div>





                        <!-- Edit Game Level Button -->
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" class="btn btn-default">
                                    <i class="fa fa-btn fa-save"></i>Save
                                </button>
                            </div>
                        </div>
                    </form>

                    <div class="form-group">
                        <label for="game-description" class="col-sm-3 control-label">
                            Matrix Size edit per 5 level
                        </label>

                        <div class="col-sm-8">
                            <form action="<?php echo e(url('manageGameLevel/game-level-detail')); ?>" method="GET">
                                <input type="hidden" name="game_id" value="<?php echo e($game->id); ?>">
                                <input type="hidden" name="block" value="2">
                                <?php /*<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">*/ ?>
                                <button type="submit" id="<?php echo e($game->id); ?>" class="btn btn-danger">
                                    <i class="fa fa-btn fa-edit"></i>Edit
                                </button>
                            </form>
                        </div>
                    </div>

                </div>
            </div>

        </div>

        <div class="col-sm-offset-1 col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo e($game->gameName); ?> - Level 41-60
                </div>

                <div class="panel-body">
                    <!-- Display Validation Errors -->
                    <!-- Edit Game Level Form -->
                    <form action="<?php echo e(url('manageGameLevel/game-level')); ?>" method="POST" class="form-horizontal">
                        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                        <input type="hidden" name="game_id" value="<?php echo e($game->id); ?>">
                        <input type="hidden" name="level" value="41">
                        <!-- Inital response time -->
                        <div class="form-group">
                            <label for="game-startTime" class="col-sm-3 control-label">Start of response time</label>

                            <div class="col-sm-8">
                                <input type="text" name="startTime" id="game-startTime" class="form-control"
                                       value="<?php echo e($gamelevel[41]->duration); ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="game-decreaseTime" class="col-sm-3 control-label">Decreasing time per
                                level</label>

                            <div class="col-sm-8">
                                <input type="text" name="decreaseTime" id="game-decreaseTime" class="form-control"
                                       value="<?php echo e($difference[41]); ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="game-description" class="col-sm-3 control-label">
                                Character Set
                            </label>

                            <div class="col-sm-8">
                                <?php if($characterSets->isEmpty()): ?>
                                    <div class="alert alert-danger">
                                        Does exist any character set
                                    </div>
                                <?php else: ?>
                                    <select name="charaterset_id" class="form-control">

                                        <?php foreach($characterSets as $characterSet): ?>
                                            <?php if($characterSet->isCompleted()==true): ?>
                                                <?php if($gamelevel[41]->characterSet_id==$characterSet->id): ?>
                                                    <option value="<?php echo e($characterSet->id); ?>"
                                                            selected><?php echo e($characterSet->characterSetName); ?></option>
                                                <?php else: ?>
                                                    <option value="<?php echo e($characterSet->id); ?>"><?php echo e($characterSet->characterSetName); ?></option>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        <?php endforeach; ?>

                                    </select>
                                <?php endif; ?>
                            </div>
                        </div>



                        <!-- Edit Game Level Button -->
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" class="btn btn-default">
                                    <i class="fa fa-btn fa-save"></i>Save
                                </button>
                            </div>
                        </div>
                    </form>

                    <div class="form-group">
                        <label for="game-description" class="col-sm-3 control-label">
                            Matrix Size edit per 5 level
                        </label>

                        <div class="col-sm-8">
                            <form action="<?php echo e(url('manageGameLevel/game-level-detail')); ?>" method="GET">
                                <input type="hidden" name="game_id" value="<?php echo e($game->id); ?>">
                                <input type="hidden" name="block" value="3">
                                <?php /*<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">*/ ?>
                                <button type="submit" id="<?php echo e($game->id); ?>" class="btn btn-danger">
                                    <i class="fa fa-btn fa-edit"></i>Edit
                                </button>
                            </form>
                        </div>
                    </div>

                </div>
            </div>

        </div>

    </div>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>