<?php if(count($errors) > 0): ?>
    <!-- Form Error List -->
    <div class="alert alert-danger">
        <strong>Whoops! Something went wrong!</strong>

        <br><br>

        <ul>
            <?php foreach($errors->all() as $error): ?>
                <li><?php echo e($error); ?></li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif; ?>


<?php if(Session::has('error_term')): ?>
    <div class="alert alert-danger alert-flash">
        <ul>
            <li><?php echo e(Session::get('error_term')); ?></li>
        </ul>
    </div>

<?php endif; ?>
