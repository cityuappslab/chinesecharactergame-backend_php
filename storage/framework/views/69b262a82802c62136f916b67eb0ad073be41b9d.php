<?php $__env->startSection('content'); ?>
	<div class="container">
		<div class="col-sm-offset-1 col-sm-10">

			<!-- Current games -->
			<?php if(count($games) > 0): ?>
				<div class="panel panel-default">
					<div class="panel-heading">
						Current Game
					</div>

					<div class="panel-body">
						<table class="table table-striped game-table">
							<thead>
								<th>Game</th>
								<th>&nbsp;</th>
							</thead>
							<tbody>
								<?php foreach($games as $game): ?>
									<tr>
										<td class="table-text"><div><?php echo e($game->gameName); ?></div></td>

										<!-- gamelevel Button -->
										<td>
											<form action="<?php echo e(url('manageGameLevel/game-level')); ?>" method="GET">
												<input type="hidden" name="id" value="<?php echo e($game->id); ?>">
												<?php /*<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">*/ ?>
												<button type="submit" id="<?php echo e($game->id); ?>" class="btn btn-danger">
													<i class="fa fa-btn fa-edit"></i>Edit
												</button>
											</form>
										</td>

										<!-- game Delete Button -->
										<!-- <td>
											<form action="<?php echo e(url('manageGame/delete-game')); ?>" method="POST">
												<input type="hidden" name="id" value="<?php echo e($game->id); ?>">
												<?php /*<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">*/ ?>
												<button type="submit" id="<?php echo e($game->id); ?>" class="btn btn-danger">
													<i class="fa fa-btn fa-trash"></i>Delete
												</button>
											</form>
										</td> -->
									</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>