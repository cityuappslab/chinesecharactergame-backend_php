<?php $__env->startSection('content'); ?>
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Register</div>
                    <div class="panel-body">
                        <?php echo Form::model($user,['method'=>'POST','url'=>['userinfo/update']],['class'=>'form-horizontal']); ?>


                        <div class="form-group">
                            <?php echo Form::label('name','Name: ',['class'=>'col-md-4 control-label']); ?>

                            <?php echo Form::text('name',null,['class'=>'form-controle']); ?>

                        </div>


                        <div class="form-group">
                            <?php echo Form::label('self_into','Self Introduction: ',['class'=>'col-md-4 control-label']); ?>

                            <?php echo Form::textarea('self_into',null,['class'=>'form-controle']); ?>

                        </div>

                        <div class="form-group">
                            <?php echo Form::label('photo_url','Person Picture: ',['class'=>'col-md-4 control-label']); ?>

                            <?php echo Form::file('image'); ?>

                        </div>


                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i>Update Profile
                                </button>
                            </div>
                        </div>
                        <?php echo Form::close(); ?>

                    </div>
                    <?php if(isset($result)): ?>
                        <div class="alert alert-success">
                            <strong><?php echo e($result); ?></strong>
                        </div>
                    <?php endif; ?>
                    <?php echo $__env->make('common.errors', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>