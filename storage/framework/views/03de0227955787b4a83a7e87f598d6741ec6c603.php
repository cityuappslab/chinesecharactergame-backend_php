<?php $__env->startSection('content'); ?>

    <div class="container">
        <div class="row">

        <?php foreach($data as $user): ?>
            <!-- Card Projects -->
                <div class="col-md-7 col-md-offset-3">
                    <div class="card">
                        <div class="card-content btn-primary">
                            <table class="table-condensed">
                                <tbody>
                                <tr class="info">
                                    <th>Name:</th>
                                    <td><?php echo e($user['name']); ?>&nbsp</td>
                                </tr>
                                <tr class="info">
                                    <th>Email:</th>
                                    <td><?php echo e($user['email']); ?>&nbsp</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <a href="<?php echo e(url("records/user?id=".$user['id'])); ?>">
                            <div class="card-action bg-info">
                                Detail
                            </div>
                        </a>


                        <a href="<?php echo e(url('exportuser/user?id='.$user['id'])); ?>" class="btn btn-primary pull-right">Export User Record</a>





                    </div>
                </div>
            <?php endforeach; ?>


        </div>

        <br/>

        <div class="row">
            <div class="col-md-7 col-md-offset-3">
                <?php if($prev_page_url!=null): ?>
                    <a type="button" class="btn btn-link" href="<?php echo e($prev_page_url); ?>">previous page</a>
                <?php endif; ?>

                <?php if($next_page_url!=null): ?>
                    <a type="button" class="btn btn-link pull-right" href="<?php echo e($next_page_url); ?>">next page</a>
                <?php endif; ?>
            </div>
        </div>

        <br/><br/><br/>


    </div>

    <style>
        .card {
            box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
        }

        .card {
            margin-top: 10px;
            box-sizing: border-box;
            border-radius: 2px;
            background-clip: padding-box;
        }

        .card span.card-title {
            color: #fff;
            font-size: 24px;
            font-weight: 300;
            text-transform: uppercase;
        }

        .card .card-image {
            position: relative;
            overflow: hidden;
        }

        .card .card-image img {
            border-radius: 2px 2px 0 0;
            background-clip: padding-box;
            position: relative;
            z-index: -1;
        }

        .card .card-image span.card-title {
            position: absolute;
            bottom: 0;
            left: 0;
            padding: 16px;
        }

        .card .card-content {
            padding: 16px;
            border-radius: 0 0 2px 2px;
            background-clip: padding-box;
            box-sizing: border-box;
        }

        .card .card-content p {
            margin: 0;
            color: inherit;
        }

        .card .card-content span.card-title {
            line-height: 48px;
        }

        .card .card-action {
            border-top: 1px solid rgba(160, 160, 160, 0.2);
            padding: 16px;
        }

        .card .card-action a {
            color: #ffab40;
            margin-right: 16px;
            transition: color 0.3s ease;
            text-transform: uppercase;
        }

        .card .card-action a:hover {
            color: #ffd8a6;
            text-decoration: none;
        }
    </style>


<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>