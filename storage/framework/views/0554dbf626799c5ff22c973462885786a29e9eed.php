<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <link rel="shortcut icon" href="http://sstatic.net/stackoverflow/img/favicon.ico">
    <title>Bird Watcher</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet'
          type='text/css'>

    <link href="<?php echo e(url('assets/css/bootstrap.css')); ?>" rel="stylesheet">

    <link href="<?php echo e(url('assets/css/facebook.css')); ?>" rel="stylesheet">
</head>

<body>

<div class="wrapper">
    <div class="box">
        <div class="row row-offcanvas row-offcanvas-left">


            <!-- main right col -->
            <div class="column col-sm-12 col-xs-12" id="main">

                <!-- top nav -->
                <div class="navbar navbar-blue navbar-static-top">
                    <div class="navbar-header  col-lg-offset-3">
                        <button class="navbar-toggle" type="button" data-toggle="collapse"
                                data-target=".navbar-collapse">
                            <span class="sr-only">Toggle</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a href="<?php echo e(url('tasks')); ?>" class="navbar-brand logo">A</a>
                    </div>
                    <nav class="collapse navbar-collapse" role="navigation">
                        <form class="navbar-form navbar-left">
                            <div class="input-group input-group-sm" style="max-width:360px;">
                                <input class="form-control" placeholder="Search" name="srch-term" id="srch-term"
                                       type="text">
                                <div class="input-group-btn">
                                    <button class="btn btn-default" type="submit"><i
                                                class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </form>
                        <ul class="nav navbar-nav navbar-right">
                            <?php if(!Auth::guest()&&Auth::user()->getProfileImage()!=false): ?>
                                <li href="<?php echo e(url('home')); ?>">
                                    <a href="<?php echo e(url('home')); ?>">
                                    <img src=<?php echo e(Auth::user()->getSmallProfileImage()); ?> style="padding:5"/>
                                    </a>
                                </li>
                            <?php endif; ?>
                            <li>
                                <a href="<?php echo e(url('home')); ?>">
                                    <i class="fa fa-home"></i>
                                    Home
                                </a>
                            </li>
                            <?php if(!Auth::guest()): ?>
                                <li>
                                    <a href="#postModal" role="button" data-toggle="modal">
                                        <i class="fa fa-plus"></i>
                                        Post
                                    </a>
                                </li>
                            <?php endif; ?>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                                            class="fa fa-user "></i></a>
                                <ul class="dropdown-menu">
                                    <?php if(Auth::guest()): ?>
                                        <li><a href="<?php echo e(url('/login')); ?>">Login</a></li>
                                        <li><a href="<?php echo e(url('/register')); ?>">Register</a></li>
                                    <?php else: ?>
                                        <?php if(Auth::user()->isAdmin()): ?>
                                            <li><a href="<?php echo e(url('/Admin/')); ?>">
                                                    <i class=" fa fa-btn fa-user-secret"></i>
                                                    Admin Page
                                                </a>
                                            </li>
                                        <?php endif; ?>
                                        <li><a href="<?php echo e(url('/userinfo/update')); ?>">
                                                <i class=" fa fa-btn fa-cogs"></i>
                                                Setting
                                            </a>
                                        </li>
                                        <li><a href="<?php echo e(url('/logout')); ?>">
                                                <i class="fa fa-btn fa-sign-out"></i>
                                                Logout
                                            </a>
                                    <?php endif; ?>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
                <!-- /top nav -->

                <div class="padding">
                    <div class="full col-sm-9">

                        <!-- content -->
                        <div class="row">
                            <?php echo $__env->yieldContent('content'); ?>
                        </div>
                        <!--/row-->


                    </div><!-- /col-9 -->
                </div><!-- /padding -->
            </div>
            <!-- /main -->

        </div>
    </div>
</div>


<!--post modal-->
<div id="postModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
                Update Status
            </div>
            <div class="modal-body">
                <form class="form center-block">
                    <div class="form-group">
                        <textarea class="form-control input-lg" autofocus=""
                                  placeholder="What do you want to share?"></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div>
                    <button class="btn btn-primary btn-sm" data-dismiss="modal" aria-hidden="true">Post</button>
                    <ul class="pull-left list-inline">
                        <li><a href=""><i class="fa fa-upload"></i></a></li>
                        <li><a href=""><i class="fa fa-camera"></i></a></li>
                        <li><a href=""><i class="fa fa-map-marker"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo e(url('assets/js/jquery.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(url('assets/js/bootstrap.js')); ?>"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('[data-toggle=offcanvas]').click(function () {
            $(this).toggleClass('visible-xs text-center');
            $('.row-offcanvas').toggleClass('active');
            $('#lg-menu').toggleClass('hidden-xs').toggleClass('visible-xs');
            $('#xs-menu').toggleClass('visible-xs').toggleClass('hidden-xs');
            $('#btnShow').toggle();
        });
    });
</script>
</body>
</html>