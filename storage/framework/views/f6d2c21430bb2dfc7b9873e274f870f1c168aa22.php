<?php $__env->startSection('content'); ?>

<?php $__env->startSection('maintainScroll'); ?>
    (function($){
    window.onbeforeunload = function(e){    
    window.name += ' [' + $(window).scrollTop().toString() + '[' + $(window).scrollLeft().toString();
    };
    $.maintainscroll = function() {
    if(window.name.indexOf('[') > 0)
    {
    var parts = window.name.split('['); 
    window.name = $.trim(parts[0]);
    window.scrollTo(parseInt(parts[parts.length - 1]), parseInt(parts[parts.length - 2]));
    }   
    };  
    $.maintainscroll();
    })(jQuery);
<?php $__env->stopSection(); ?>

    <div class="container">
        <div class="col-sm-offset-1 col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo e($game->gameName); ?> - Character Set
                </div>
                <div class="panel-body">

                <form action="<?php echo e(url('manageGameLevel/game-level-character')); ?>" method="POST" class="form-horizontal">
                    <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                    <input type="hidden" name="game_id" value="<?php echo e($game->id); ?>">
                    <input type="hidden" name="level" value="1">

                    <div class="form-group">
                            <label for="game-description" class="col-sm-3 control-label">
                                Character Set for Level 1 - 20
                            </label>

                            <div class="col-sm-8">
                                <?php if($characterSets->isEmpty()): ?>
                                    <div class="alert alert-danger">
                                        Does exist any character set
                                    </div>
                                <?php else: ?>
                                    <select name="charaterset_id" class="form-control">

                                        <?php foreach($characterSets as $characterSet): ?>
                                            <?php if($characterSet->isCompleted()==true): ?>
                                                <?php if($gamelevel[1]->characterSet_id==$characterSet->id): ?>
                                                    <option value="<?php echo e($characterSet->id); ?>"
                                                            selected><?php echo e($characterSet->characterSetName); ?></option>
                                                <?php else: ?>
                                                    <option value="<?php echo e($characterSet->id); ?>"><?php echo e($characterSet->characterSetName); ?></option>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        <?php endforeach; ?>

                                    </select>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="form-group">

                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-save"></i>Save

                                </button>
                            </div>
                        </div>

                </form>

                    <form action="<?php echo e(url('manageGameLevel/game-level-character')); ?>" method="POST" class="form-horizontal">
                    <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                    <input type="hidden" name="game_id" value="<?php echo e($game->id); ?>">
                    <input type="hidden" name="level" value="21">

                    <div class="form-group">
                            <label for="game-description" class="col-sm-3 control-label">Character Set for Level 21 - 40
                            </label>

                            <div class="col-sm-8">
                                <?php if($characterSets->isEmpty()): ?>
                                    <div class="alert alert-danger">
                                        Does exist any character set
                                    </div>
                                <?php else: ?>
                                    <select name="charaterset_id" class="form-control">

                                        <?php foreach($characterSets as $characterSet): ?>
                                            <?php if($characterSet->isCompleted()==true): ?>
                                                <?php if($gamelevel[21]->characterSet_id==$characterSet->id): ?>
                                                    <option value="<?php echo e($characterSet->id); ?>"
                                                            selected><?php echo e($characterSet->characterSetName); ?></option>
                                                <?php else: ?>
                                                    <option value="<?php echo e($characterSet->id); ?>"><?php echo e($characterSet->characterSetName); ?></option>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        <?php endforeach; ?>

                                    </select>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="form-group">

                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-save"></i>Save

                                </button>
                            </div>
                        </div>

                </form>

                    <form action="<?php echo e(url('manageGameLevel/game-level-character')); ?>" method="POST" class="form-horizontal">
                    <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                    <input type="hidden" name="game_id" value="<?php echo e($game->id); ?>">
                    <input type="hidden" name="level" value="41">

                    <div class="form-group">
                            <label for="game-description" class="col-sm-3 control-label">Character Set for Level 41 - 60
                            </label>

                            <div class="col-sm-8">
                                <?php if($characterSets->isEmpty()): ?>
                                    <div class="alert alert-danger">
                                        Does exist any character set
                                    </div>
                                <?php else: ?>
                                    <select name="charaterset_id" class="form-control">

                                        <?php foreach($characterSets as $characterSet): ?>
                                            <?php if($characterSet->isCompleted()==true): ?>
                                                <?php if($gamelevel[41]->characterSet_id==$characterSet->id): ?>
                                                    <option value="<?php echo e($characterSet->id); ?>"
                                                            selected><?php echo e($characterSet->characterSetName); ?></option>
                                                <?php else: ?>
                                                    <option value="<?php echo e($characterSet->id); ?>"><?php echo e($characterSet->characterSetName); ?></option>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        <?php endforeach; ?>

                                    </select>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="form-group">

                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-save"></i>Save

                                </button>
                            </div>
                        </div>

                </form>
                    
                </div>
            </div>
        </div>
                    
        <div class="col-sm-offset-1 col-sm-12">
            <?php echo $__env->make('common.errors', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <?php if($game->isCompleted()==false): ?>
                <div class="alert alert-danger">
                    <strong>Some information of game is not completed. Please select the character set and save.</strong>
                </div>
            <?php endif; ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo e($game->gameName); ?> - Level 1-5
                </div>

                <div class="panel-body">
                    <!-- Display Validation Errors -->

                    <!-- Edit Game Level Form -->
                    <form action="<?php echo e(url('manageGameLevel/game-level')); ?>" method="POST" class="form-horizontal">
                        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                        <input type="hidden" name="game_id" value="<?php echo e($game->id); ?>">
                        <input type="hidden" name="level" value="1">

                        <!-- Inital response time -->
                        <div class="form-group">
                            <label for="game-startTime" class="col-sm-3 control-label">Start of response time (s)</label>

                            <div class="col-sm-8">
                                <input type="text" name="startTime" id="game-startTime" class="form-control"
                                       value="<?php echo e($duration[1]); ?>">
                            </div>
                        </div>

                        <?php if($game->id == 11): ?>
                            <div class="form-group">
                                <label for="game-decreaseTime" class="col-sm-3  control-label">Text Disappear Time (s)</label>
    
                                <div class="col-sm-8">
                                    <input type="text" name="disappearTime" id="    game-disappearTime" class="form-control" value="<?php echo e($disappear[1]); ?>">
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="game-decreaseTime" class="col-sm-3  control-label">Disappear time per level decrease by (%)
                                    </label>
    
                                <div class="col-sm-8">
                                    <input type="text" name="decreaseTime" id=" game-decreaseTime" class="form-control"
                                           value="<?php echo e($percent[1]); ?>">
                                </div>
                            </div>
                        <?php endif; ?>


                        <!-- Edit Game Level Button -->
                        <div class="form-group">

                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-save"></i>Save

                                </button>
                            </div>
                        </div>
                    </form>

                    <div class="form-group">
                        <label for="game-description" class="col-sm-3 control-label">
                            Matrix Size edit per 5 level
                        </label>

                        <div class="col-sm-8">
                            <form action="<?php echo e(url('manageGameLevel/game-level-detail')); ?>" method="GET">
                                <input type="hidden" name="game_id" value="<?php echo e($game->id); ?>">
                                <input type="hidden" name="block" value="1">
                                <?php /*<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">*/ ?>
                                <button type="submit" id="<?php echo e($game->id); ?>" class="btn btn-danger">
                                    <i class="fa fa-btn fa-edit"></i>Edit
                                </button>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>


        <div class="col-sm-offset-1 col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo e($game->gameName); ?> - Level 6-10
                </div>

                <div class="panel-body">
                    <!-- Display Validation Errors -->
                    <!-- Edit Game Level Form -->
                    <form action="<?php echo e(url('manageGameLevel/game-level')); ?>" method="POST" class="form-horizontal">
                        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                        <input type="hidden" name="game_id" value="<?php echo e($game->id); ?>">
                        <input type="hidden" name="level" value="6">
                        <!-- Inital response time -->
                        <div class="form-group">
                            <label for="game-startTime" class="col-sm-3 control-label">Start of response time (s)</label>

                            <div class="col-sm-8">
                                <input type="text" name="startTime" id="game-startTime" class="form-control"
                                       value="<?php echo e($gamelevel[6]->duration); ?>">
                            </div>
                        </div>

                        <?php if($game->id == 11): ?>
                            <div class="form-group">
                                <label for="game-decreaseTime" class="col-sm-3  control-label">Text Disappear Time (s)</label>
    
                                <div class="col-sm-8">
                                    <input type="text" name="disappearTime" id="    game-disappearTime" class="form-control" value="<?php echo e($disappear[6]); ?>">
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="game-decreaseTime" class="col-sm-3  control-label">Disappear time per level decrease by (%)
                                    </label>
    
                                <div class="col-sm-8">
                                    <input type="text" name="decreaseTime" id=" game-decreaseTime" class="form-control" value="<?php echo e($percent[6]); ?>">
                                </div>
                            </div>
                        <?php endif; ?>


                        <!-- Edit Game Level Button -->
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-save"></i>Save
                                </button>
                            </div>
                        </div>
                    </form>

                    <div class="form-group">
                        <label for="game-description" class="col-sm-3 control-label">
                            Matrix Size edit per 5 level
                        </label>

                        <div class="col-sm-8">
                            <form action="<?php echo e(url('manageGameLevel/game-level-detail')); ?>" method="GET">
                                <input type="hidden" name="game_id" value="<?php echo e($game->id); ?>">
                                <input type="hidden" name="block" value="2">
                                <?php /*<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">*/ ?>
                                <button type="submit" id="<?php echo e($game->id); ?>" class="btn btn-danger">
                                    <i class="fa fa-btn fa-edit"></i>Edit
                                </button>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>


        <div class="col-sm-offset-1 col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">

                    <?php echo e($game->gameName); ?> - Level 11-15
                </div>

                <div class="panel-body">
                    <!-- Display Validation Errors -->
                    <!-- Edit Game Level Form -->
                    <form action="<?php echo e(url('manageGameLevel/game-level')); ?>" method="POST" class="form-horizontal">
                        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                        <input type="hidden" name="game_id" value="<?php echo e($game->id); ?>">
                        <input type="hidden" name="level" value="11">
                        <!-- Inital response time -->
                        <div class="form-group">
                            <label for="game-startTime" class="col-sm-3 control-label">Start of response time (s)</label>

                            <div class="col-sm-8">
                                <input type="text" name="startTime" id="game-startTime" class="form-control"
                                       value="<?php echo e($gamelevel[11]->duration); ?>">
                            </div>
                        </div>

                        <?php if($game->id == 11): ?>

                        <div class="form-group">
                            <label for="game-decreaseTime" class="col-sm-3 control-label">Text Disappear Time (s)</label>

                            <div class="col-sm-8">
                                <input type="text" name="disappearTime" id="game-disappearTime" class="form-control"
                                       value="<?php echo e($disappear[11]); ?>">
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="game-decreaseTime" class="col-sm-3 control-label">Disappear time per level decrease by (%)
                                </label>

                            <div class="col-sm-8">
                                <input type="text" name="decreaseTime" id="game-decreaseTime" class="form-control"
                                       value="<?php echo e($percent[11]); ?>">
                            </div>
                        </div>

                        <?php endif; ?>


                        <!-- Edit Game Level Button -->
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-save"></i>Save
                                </button>
                            </div>
                        </div>
                    </form>

                    <div class="form-group">
                        <label for="game-description" class="col-sm-3 control-label">
                            Matrix Size edit per 5 level
                        </label>

                        <div class="col-sm-8">
                            <form action="<?php echo e(url('manageGameLevel/game-level-detail')); ?>" method="GET">
                                <input type="hidden" name="game_id" value="<?php echo e($game->id); ?>">
                                <input type="hidden" name="block" value="3">
                                <?php /*<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">*/ ?>
                                <button type="submit" id="<?php echo e($game->id); ?>" class="btn btn-danger">
                                    <i class="fa fa-btn fa-edit"></i>Edit
                                </button>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>


         <div class="col-sm-offset-1 col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo e($game->gameName); ?> - Level 16-20
                </div>

                <div class="panel-body">
                    <!-- Display Validation Errors -->
                    <!-- Edit Game Level Form -->
                    <form action="<?php echo e(url('manageGameLevel/game-level')); ?>" method="POST" class="form-horizontal">
                        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                        <input type="hidden" name="game_id" value="<?php echo e($game->id); ?>">
                        <input type="hidden" name="level" value="16">
                        <!-- Inital response time -->
                        <div class="form-group">
                            <label for="game-startTime" class="col-sm-3 control-label">Start of response time (s)</label>

                            <div class="col-sm-8">
                                <input type="text" name="startTime" id="game-startTime" class="form-control"
                                       value="<?php echo e($gamelevel[16]->duration); ?>">
                            </div>
                        </div>

                        <?php if($game->id == 11): ?>

                        <div class="form-group">
                            <label for="game-decreaseTime" class="col-sm-3 control-label">Text Disappear Time (s)</label>

                            <div class="col-sm-8">
                                <input type="text" name="disappearTime" id="game-disappearTime" class="form-control"
                                       value="<?php echo e($disappear[16]); ?>">
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="game-decreaseTime" class="col-sm-3 control-label">Disappear time per level decrease by (%)
                                </label>

                            <div class="col-sm-8">
                                <input type="text" name="decreaseTime" id="game-decreaseTime" class="form-control"
                                       value="<?php echo e($percent[16]); ?>">
                            </div>
                        </div>

                        <?php endif; ?>


                        <!-- Edit Game Level Button -->
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-save"></i>Save
                                </button>
                            </div>
                        </div>
                    </form>

                    <div class="form-group">
                        <label for="game-description" class="col-sm-3 control-label">
                            Matrix Size edit per 5 level
                        </label>

                        <div class="col-sm-8">
                            <form action="<?php echo e(url('manageGameLevel/game-level-detail')); ?>" method="GET">
                                <input type="hidden" name="game_id" value="<?php echo e($game->id); ?>">
                                <input type="hidden" name="block" value="4">
                                <?php /*<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">*/ ?>
                                <button type="submit" id="<?php echo e($game->id); ?>" class="btn btn-danger">
                                    <i class="fa fa-btn fa-edit"></i>Edit
                                </button>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>


        <div class="col-sm-offset-1 col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo e($game->gameName); ?> - Level 21-25
                </div>

                <div class="panel-body">
                    <!-- Display Validation Errors -->
                    <!-- Edit Game Level Form -->
                    <form action="<?php echo e(url('manageGameLevel/game-level')); ?>" method="POST" class="form-horizontal">
                        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                        <input type="hidden" name="game_id" value="<?php echo e($game->id); ?>">
                        <input type="hidden" name="level" value="21">
                        <!-- Inital response time -->
                        <div class="form-group">
                            <label for="game-startTime" class="col-sm-3 control-label">Start of response time (s)</label>

                            <div class="col-sm-8">
                                <input type="text" name="startTime" id="game-startTime" class="form-control"
                                       value="<?php echo e($gamelevel[21]->duration); ?>">
                            </div>
                        </div>

                        <?php if($game->id == 11): ?>

                        <div class="form-group">
                            <label for="game-decreaseTime" class="col-sm-3 control-label">Text Disappear Time (s)</label>

                            <div class="col-sm-8">
                                <input type="text" name="disappearTime" id="game-disappearTime" class="form-control"
                                       value="<?php echo e($disappear[21]); ?>">
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="game-decreaseTime" class="col-sm-3 control-label">Disappear time per level decrease by (%)
                                </label>

                            <div class="col-sm-8">
                                <input type="text" name="decreaseTime" id="game-decreaseTime" class="form-control"
                                       value="<?php echo e($percent[21]); ?>">
                            </div>
                        </div>

                        <?php endif; ?>

                        <!-- Edit Game Level Button -->
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-save"></i>Save
                                </button>
                            </div>
                        </div>
                    </form>

                    <div class="form-group">
                        <label for="game-description" class="col-sm-3 control-label">
                            Matrix Size edit per 5 level
                        </label>

                        <div class="col-sm-8">
                            <form action="<?php echo e(url('manageGameLevel/game-level-detail')); ?>" method="GET">
                                <input type="hidden" name="game_id" value="<?php echo e($game->id); ?>">
                                <input type="hidden" name="block" value="5">
                                <?php /*<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">*/ ?>
                                <button type="submit" id="<?php echo e($game->id); ?>" class="btn btn-danger">
                                    <i class="fa fa-btn fa-edit"></i>Edit
                                </button>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>


        <div class="col-sm-offset-1 col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo e($game->gameName); ?> - Level 26-30
                </div>

                <div class="panel-body">
                    <!-- Display Validation Errors -->
                    <!-- Edit Game Level Form -->
                    <form action="<?php echo e(url('manageGameLevel/game-level')); ?>" method="POST" class="form-horizontal">
                        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                        <input type="hidden" name="game_id" value="<?php echo e($game->id); ?>">
                        <input type="hidden" name="level" value="26">
                        <!-- Inital response time -->
                        <div class="form-group">
                            <label for="game-startTime" class="col-sm-3 control-label">Start of response time (s)</label>

                            <div class="col-sm-8">
                                <input type="text" name="startTime" id="game-startTime" class="form-control"
                                       value="<?php echo e($gamelevel[26]->duration); ?>">
                            </div>
                        </div>

                        <?php if($game->id == 11): ?>

                        <div class="form-group">
                            <label for="game-decreaseTime" class="col-sm-3 control-label">Text Disappear Time (s)</label>

                            <div class="col-sm-8">
                                <input type="text" name="disappearTime" id="game-disappearTime" class="form-control"
                                       value="<?php echo e($disappear[26]); ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="game-decreaseTime" class="col-sm-3 control-label">Disappear time per level decrease by (%)
                                </label>

                            <div class="col-sm-8">
                                <input type="text" name="decreaseTime" id="game-decreaseTime" class="form-control"
                                       value="<?php echo e($percent[26]); ?>">
                            </div>
                        </div>

                        <?php endif; ?>

                        <!-- Edit Game Level Button -->
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-save"></i>Save
                                </button>
                            </div>
                        </div>
                    </form>

                    <div class="form-group">
                        <label for="game-description" class="col-sm-3 control-label">
                            Matrix Size edit per 5 level
                        </label>

                        <div class="col-sm-8">
                            <form action="<?php echo e(url('manageGameLevel/game-level-detail')); ?>" method="GET">
                                <input type="hidden" name="game_id" value="<?php echo e($game->id); ?>">
                                <input type="hidden" name="block" value="6">
                                <?php /*<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">*/ ?>
                                <button type="submit" id="<?php echo e($game->id); ?>" class="btn btn-danger">
                                    <i class="fa fa-btn fa-edit"></i>Edit
                                </button>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>


        
                <div class="col-sm-offset-1 col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo e($game->gameName); ?> - Level 31-35
                </div>

                <div class="panel-body">
                    <!-- Display Validation Errors -->
                    <!-- Edit Game Level Form -->
                    <form action="<?php echo e(url('manageGameLevel/game-level')); ?>" method="POST" class="form-horizontal">
                        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                        <input type="hidden" name="game_id" value="<?php echo e($game->id); ?>">
                        <input type="hidden" name="level" value="31">
                        <!-- Inital response time -->
                        <div class="form-group">
                            <label for="game-startTime" class="col-sm-3 control-label">Start of response time (s)</label>

                            <div class="col-sm-8">
                                <input type="text" name="startTime" id="game-startTime" class="form-control"
                                       value="<?php echo e($gamelevel[31]->duration); ?>">
                            </div>
                        </div>

                        <?php if($game->id == 11): ?>

                        <div class="form-group">
                            <label for="game-decreaseTime" class="col-sm-3 control-label">Text Disappear Time (s)</label>

                            <div class="col-sm-8">
                                <input type="text" name="disappearTime" id="game-disappearTime" class="form-control"
                                       value="<?php echo e($disappear[31]); ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="game-decreaseTime" class="col-sm-3 control-label">Disappear time per level decrease by (%)
                                </label>

                            <div class="col-sm-8">
                                <input type="text" name="decreaseTime" id="game-decreaseTime" class="form-control"
                                       value="<?php echo e($percent[31]); ?>">
                            </div>
                        </div>

                        <?php endif; ?>

                        <!-- Edit Game Level Button -->
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-save"></i>Save
                                </button>
                            </div>
                        </div>
                    </form>

                    <div class="form-group">
                        <label for="game-description" class="col-sm-3 control-label">
                            Matrix Size edit per 5 level
                        </label>

                        <div class="col-sm-8">
                            <form action="<?php echo e(url('manageGameLevel/game-level-detail')); ?>" method="GET">
                                <input type="hidden" name="game_id" value="<?php echo e($game->id); ?>">
                                <input type="hidden" name="block" value="7">
                                <?php /*<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">*/ ?>
                                <button type="submit" id="<?php echo e($game->id); ?>" class="btn btn-danger">
                                    <i class="fa fa-btn fa-edit"></i>Edit
                                </button>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>


        <div class="col-sm-offset-1 col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo e($game->gameName); ?> - Level 36-40
                </div>

                <div class="panel-body">
                    <!-- Display Validation Errors -->
                    <!-- Edit Game Level Form -->
                    <form action="<?php echo e(url('manageGameLevel/game-level')); ?>" method="POST" class="form-horizontal">
                        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                        <input type="hidden" name="game_id" value="<?php echo e($game->id); ?>">
                        <input type="hidden" name="level" value="36">
                        <!-- Inital response time -->
                        <div class="form-group">
                            <label for="game-startTime" class="col-sm-3 control-label">Start of response time (s)</label>

                            <div class="col-sm-8">
                                <input type="text" name="startTime" id="game-startTime" class="form-control"
                                       value="<?php echo e($gamelevel[36]->duration); ?>">
                            </div>
                        </div>

                        <?php if($game->id == 11): ?>

                        <div class="form-group">
                            <label for="game-decreaseTime" class="col-sm-3 control-label">Text Disappear Time (s)</label>

                            <div class="col-sm-8">
                                <input type="text" name="disappearTime" id="game-disappearTime" class="form-control"
                                       value="<?php echo e($disappear[36]); ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="game-decreaseTime" class="col-sm-3 control-label">Disappear time per level decrease by (%)
                                </label>

                            <div class="col-sm-8">
                                <input type="text" name="decreaseTime" id="game-decreaseTime" class="form-control"
                                       value="<?php echo e($percent[36]); ?>">
                            </div>
                        </div>

                        <?php endif; ?>

                        <!-- Edit Game Level Button -->
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-save"></i>Save
                                </button>
                            </div>
                        </div>
                    </form>

                    <div class="form-group">
                        <label for="game-description" class="col-sm-3 control-label">
                            Matrix Size edit per 5 level
                        </label>

                        <div class="col-sm-8">
                            <form action="<?php echo e(url('manageGameLevel/game-level-detail')); ?>" method="GET">
                                <input type="hidden" name="game_id" value="<?php echo e($game->id); ?>">
                                <input type="hidden" name="block" value="8">
                                <?php /*<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">*/ ?>
                                <button type="submit" id="<?php echo e($game->id); ?>" class="btn btn-danger">
                                    <i class="fa fa-btn fa-edit"></i>Edit
                                </button>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>



        <div class="col-sm-offset-1 col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo e($game->gameName); ?> - Level 41-45
                </div>

                <div class="panel-body">
                    <!-- Display Validation Errors -->
                    <!-- Edit Game Level Form -->
                    <form action="<?php echo e(url('manageGameLevel/game-level')); ?>" method="POST" class="form-horizontal">
                        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                        <input type="hidden" name="game_id" value="<?php echo e($game->id); ?>">
                        <input type="hidden" name="level" value="41">
                        <!-- Inital response time -->
                        <div class="form-group">
                            <label for="game-startTime" class="col-sm-3 control-label">Start of response time (s)</label>

                            <div class="col-sm-8">
                                <input type="text" name="startTime" id="game-startTime" class="form-control"
                                       value="<?php echo e($gamelevel[41]->duration); ?>">
                            </div>
                        </div>

                        <?php if($game->id  == 11): ?>

                        <div class="form-group">
                            <label for="game-decreaseTime" class="col-sm-3 control-label">Text Disappear Time (s)</label>

                            <div class="col-sm-8">
                                <input type="text" name="disappearTime" id="game-disappearTime" class="form-control"
                                       value="<?php echo e($disappear[41]); ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="game-decreaseTime" class="col-sm-3 control-label">Disappear time per level decrease by (%)
                                </label>

                            <div class="col-sm-8">
                                <input type="text" name="decreaseTime" id="game-decreaseTime" class="form-control"
                                       value="<?php echo e($percent[41]); ?>">
                            </div>
                        </div>

                        <?php endif; ?>

                        <!-- Edit Game Level Button -->
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-save"></i>Save
                                </button>
                            </div>
                        </div>
                    </form>

                    <div class="form-group">
                        <label for="game-description" class="col-sm-3 control-label">
                            Matrix Size edit per 5 level
                        </label>

                        <div class="col-sm-8">
                            <form action="<?php echo e(url('manageGameLevel/game-level-detail')); ?>" method="GET">
                                <input type="hidden" name="game_id" value="<?php echo e($game->id); ?>">
                                <input type="hidden" name="block" value="9">
                                <?php /*<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">*/ ?>
                                <button type="submit" id="<?php echo e($game->id); ?>" class="btn btn-danger">
                                    <i class="fa fa-btn fa-edit"></i>Edit
                                </button>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="col-sm-offset-1 col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo e($game->gameName); ?> - Level 46-50
                </div>

                <div class="panel-body">
                    <!-- Display Validation Errors -->
                    <!-- Edit Game Level Form -->
                    <form action="<?php echo e(url('manageGameLevel/game-level')); ?>" method="POST" class="form-horizontal">
                        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                        <input type="hidden" name="game_id" value="<?php echo e($game->id); ?>">
                        <input type="hidden" name="level" value="46">
                        <!-- Inital response time -->
                        <div class="form-group">
                            <label for="game-startTime" class="col-sm-3 control-label">Start of response time (s)</label>

                            <div class="col-sm-8">
                                <input type="text" name="startTime" id="game-startTime" class="form-control"
                                       value="<?php echo e($gamelevel[46]->duration); ?>">
                            </div>
                        </div>

                        <?php if($game->id == 11): ?>
                        <div class="form-group">
                            <label for="game-decreaseTime" class="col-sm-3 control-label">Text Disappear Time (s)</label>

                            <div class="col-sm-8">
                                <input type="text" name="disappearTime" id="game-disappearTime" class="form-control"
                                       value="<?php echo e($disappear[46]); ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="game-decreaseTime" class="col-sm-3 control-label">Disappear time per level decrease by (%)
                                </label>

                            <div class="col-sm-8">
                                <input type="text" name="decreaseTime" id="game-decreaseTime" class="form-control"
                                       value="<?php echo e($percent[46]); ?>">
                            </div>
                        </div>

                        <?php endif; ?>

                        <!-- Edit Game Level Button -->
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-save"></i>Save
                                </button>
                            </div>
                        </div>
                    </form>

                    <div class="form-group">
                        <label for="game-description" class="col-sm-3 control-label">
                            Matrix Size edit per 5 level
                        </label>

                        <div class="col-sm-8">
                            <form action="<?php echo e(url('manageGameLevel/game-level-detail')); ?>" method="GET">
                                <input type="hidden" name="game_id" value="<?php echo e($game->id); ?>">
                                <input type="hidden" name="block" value="10">
                                <?php /*<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">*/ ?>
                                <button type="submit" id="<?php echo e($game->id); ?>" class="btn btn-danger">
                                    <i class="fa fa-btn fa-edit"></i>Edit
                                </button>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>


        <div class="col-sm-offset-1 col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo e($game->gameName); ?> - Level 51-55
                </div>

                <div class="panel-body">
                    <!-- Display Validation Errors -->
                    <!-- Edit Game Level Form -->
                    <form action="<?php echo e(url('manageGameLevel/game-level')); ?>" method="POST" class="form-horizontal">
                        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                        <input type="hidden" name="game_id" value="<?php echo e($game->id); ?>">
                        <input type="hidden" name="level" value="51">
                        <!-- Inital response time -->
                        <div class="form-group">
                            <label for="game-startTime" class="col-sm-3 control-label">Start of response time (s)</label>

                            <div class="col-sm-8">
                                <input type="text" name="startTime" id="game-startTime" class="form-control"
                                       value="<?php echo e($gamelevel[51]->duration); ?>">
                            </div>
                        </div>

                        <?php if($game->id == 11): ?>

                        <div class="form-group">
                            <label for="game-decreaseTime" class="col-sm-3 control-label">Text Disappear Time (s)</label>

                            <div class="col-sm-8">
                                <input type="text" name="disappearTime" id="game-disappearTime" class="form-control"
                                       value="<?php echo e($disappear[51]); ?>">
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="game-decreaseTime" class="col-sm-3 control-label">Disappear time per level decrease by (%)
                                </label>

                            <div class="col-sm-8">
                                <input type="text" name="decreaseTime" id="game-decreaseTime" class="form-control"
                                       value="<?php echo e($percent[51]); ?>">
                            </div>
                        </div>

                        <?php endif; ?>

                        <!-- Edit Game Level Button -->
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-save"></i>Save
                                </button>
                            </div>
                        </div>
                    </form>

                    <div class="form-group">
                        <label for="game-description" class="col-sm-3 control-label">
                            Matrix Size edit per 5 level
                        </label>

                        <div class="col-sm-8">
                            <form action="<?php echo e(url('manageGameLevel/game-level-detail')); ?>" method="GET">
                                <input type="hidden" name="game_id" value="<?php echo e($game->id); ?>">
                                <input type="hidden" name="block" value="11">
                                <?php /*<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">*/ ?>
                                <button type="submit" id="<?php echo e($game->id); ?>" class="btn btn-danger">
                                    <i class="fa fa-btn fa-edit"></i>Edit
                                </button>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>







        <div class="col-sm-offset-1 col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo e($game->gameName); ?> - Level 56-60
                </div>

                <div class="panel-body">
                    <!-- Display Validation Errors -->
                    <!-- Edit Game Level Form -->
                    <form action="<?php echo e(url('manageGameLevel/game-level')); ?>" method="POST" class="form-horizontal">
                        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                        <input type="hidden" name="game_id" value="<?php echo e($game->id); ?>">
                        <input type="hidden" name="level" value="56">
                        <!-- Inital response time -->
                        <div class="form-group">
                            <label for="game-startTime" class="col-sm-3 control-label">Start of response time (s)</label>

                            <div class="col-sm-8">
                                <input type="text" name="startTime" id="game-startTime" class="form-control"
                                       value="<?php echo e($gamelevel[56]->duration); ?>">
                            </div>
                        </div>

                        <?php if($game->id == 11): ?>

                        <div class="form-group">
                            <label for="game-decreaseTime" class="col-sm-3 control-label">Text Disappear Time (s)</label>

                            <div class="col-sm-8">
                                <input type="text" name="disappearTime" id="game-disappearTime" class="form-control"
                                       value="<?php echo e($disappear[56]); ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="game-decreaseTime" class="col-sm-3 control-label">Disappear time per level decrease by (%)
                                </label>

                            <div class="col-sm-8">
                                <input type="text" name="decreaseTime" id="game-decreaseTime" class="form-control"
                                       value="<?php echo e($percent[56]); ?>">
                            </div>
                        </div>

                        <?php endif; ?>

                        <!-- Edit Game Level Button -->
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-save"></i>Save
                                </button>
                            </div>
                        </div>
                    </form>

                    <div class="form-group">
                        <label for="game-description" class="col-sm-3 control-label">
                            Matrix Size edit per 5 level
                        </label>

                        <div class="col-sm-8">
                            <form action="<?php echo e(url('manageGameLevel/game-level-detail')); ?>" method="GET">
                                <input type="hidden" name="game_id" value="<?php echo e($game->id); ?>">
                                <input type="hidden" name="block" value="12">
                                <?php /*<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">*/ ?>
                                <button type="submit" id="<?php echo e($game->id); ?>" class="btn btn-danger">
                                    <i class="fa fa-btn fa-edit"></i>Edit
                                </button>
                            </form>
                        </div>
                    </div>

                </div>
            </div>

        </div>

    </div>




<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>