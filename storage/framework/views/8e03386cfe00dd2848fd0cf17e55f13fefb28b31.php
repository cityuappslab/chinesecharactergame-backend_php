<?php $__env->startSection('content'); ?>

    <div class="col-sm-offset-1 col-sm-10">
        <div class="panel panel-default">
            <div class="panel-heading ">
                Record Dashboard
            </div>

            <div class="panel-body">
                <div class="card">

                    <div class="card-content btn-primary">
                        <table class="table-condensed table-inverse">
                            <tbody>
                            <tr class="info">
                                <th>Name:</th>
                                <td><?php echo e($user['name']); ?>&nbsp</td>
                            </tr>
                            <tr class="info">
                                <th>Email:</th>
                                <td><?php echo e($user['email']); ?>&nbsp</td>
                            </tr>
                            <tr class="info">
                                <th>Gender:</th>
                                <td><?php echo e($user['gender']); ?>&nbsp</td>
                            </tr>
                            <tr class="info">
                                <th>Date of Births:</th>
                                <td><?php echo e($user['dateOfBirths']); ?>&nbsp</td>
                            </tr>
                            <tr class="info">
                                <th>Education Level:</th>
                                <td><?php echo e($user['educationLevel']); ?>&nbsp</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    </div>

                <div class="card">

                    <div class="card-content btn-primary">
                        <table class="table-condensed table-inverse">
                            <tbody>
                            <tr class="info">
                                <th>Name:</th>
                                <td><?php echo e($game['gameName']); ?>&nbsp</td>
                            </tr>
                            <tr class="info">
                                <th>Description:</th>
                                <td><?php echo e($game['description']); ?>&nbsp</td>
                            </tr>
                            <tr class="info">
                                <th>Game Level:</th>
                                <td><?php echo e($gamelevel['level']); ?>&nbsp</td>
                            </tr>
                            <tr class="info">
                                <th>Duration:</th>
                                <td><?php echo e($gamelevel['duration']); ?>&nbsp</td>
                            </tr>
                            <tr class="info">
                                <th>NumOfRow:</th>
                                <td><?php echo e($gamelevel['numOfRow']); ?>&nbsp</td>
                            </tr>
                            <tr class="info">
                                <th>NumOfCol:</th>
                                <td><?php echo e($gamelevel['numOfCol']); ?>&nbsp</td>
                            </tr>
                            <?php /*<tr class="info">*/ ?>
                                <?php /*<th>Character Set Name:</th>*/ ?>
                                <?php /*<td><?php echo e($characterSetName); ?>&nbsp</td>*/ ?>
                            <?php /*</tr>*/ ?>
                            </tbody>
                        </table>
                    </div>
                </div>


                <div class="panel-body">


                <br/><br/>

                <!-- Card Projects -->
                    <div class="col-md-10 col-md-offset-1">
                        <table class="table table-condensed table-inverse" style="width:100%">
                            <tr>
                                <th>No.trial</th>
                                <th>Response Time</th>
                                <th>numOfRow</th>
                                <th>numOfCol</th>
                                <th>Char 1</th>
                                <th>Char 2</th>
                                <th>Result</th>
                            </tr>
                            <?php foreach($trials as $trial): ?>
                                <tr>
                                    <td><?php echo e($trial->trial); ?>&nbsp</td>
                                    <td><?php echo e($trial->responseTime); ?>&nbsp</td>
                                    <td><?php echo e($trial->numOfRow); ?>&nbsp</td>
                                    <td><?php echo e($trial->numOfCol); ?>&nbsp</td>
                                    <td><?php echo e($trial->char1); ?>&nbsp</td>
                                    <td><?php echo e($trial->char2); ?>&nbsp</td>
                                    <td><?php echo e($trial->outcome); ?>&nbsp</td>
                                </tr>
                                <?php endforeach; ?>
                                </tbody>
                        </table>
                    </div>



            </div>

        </div>
    </div>
    <br/><br/><br/>


    </div>

    <style>
        .card {
            box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
        }

        .card {
            margin-top: 10px;
            box-sizing: border-box;
            border-radius: 2px;
            background-clip: padding-box;
        }

        .card span.card-title {
            color: #fff;
            font-size: 24px;
            font-weight: 300;
            text-transform: uppercase;
        }

        .card .card-image {
            position: relative;
            overflow: hidden;
        }

        .card .card-image img {
            border-radius: 2px 2px 0 0;
            background-clip: padding-box;
            position: relative;
            z-index: -1;
        }

        .card .card-image span.card-title {
            position: absolute;
            bottom: 0;
            left: 0;
            padding: 16px;
        }

        .card .card-content {
            padding: 16px;
            border-radius: 0 0 2px 2px;
            background-clip: padding-box;
            box-sizing: border-box;
        }

        .card .card-content p {
            margin: 0;
            color: inherit;
        }

        .card .card-content span.card-title {
            line-height: 48px;
        }

        .card .card-action {
            border-top: 1px solid rgba(160, 160, 160, 0.2);
            padding: 16px;
        }

        .card .card-action a {
            color: #ffab40;
            margin-right: 16px;
            transition: color 0.3s ease;
            text-transform: uppercase;
        }

        .card .card-action a:hover {
            color: #ffd8a6;
            text-decoration: none;
        }
    </style>


<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>