<?php $__env->startSection('content'); ?>
	<div class="container">
		<div class="col-sm-offset-1 col-sm-10">
			<div class="panel panel-default">
				<div class="panel-heading">
					New Character Set
				</div>

				<div class="panel-body">
					<!-- Display Validation Errors -->
					<?php echo $__env->make('common.errors', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

					<!-- New characterSet Form -->
					<form action="/manageCharacterSet/character-set" method="POST" class="form-horizontal">
						<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
						<!-- characterSet Name -->
						<div class="form-group">
							<label for="characterSet-characterSetName" class="col-sm-3 control-label">Character Set Name</label>

							<div class="col-sm-6">
								<input type="text" name="characterSetName" id="characterSet-characterSetName" class="form-control">
							</div>
						</div>

						<?php /*<div class="form-group">*/ ?>
							<?php /*<label for="characterSet-numOfCharacter" class="col-sm-3 control-label">Number of character</label>*/ ?>

							<?php /*<div class="col-sm-6">*/ ?>
								<?php /*<input type="text" name="numOfCharacter" id="characterSet-description" class="form-control">*/ ?>
							<?php /*</div>*/ ?>
						<?php /*</div>*/ ?>

						<!-- Add characterSet Button -->
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-6">
								<button type="submit" class="btn btn-default">
									<i class="fa fa-btn fa-plus"></i>Add characterSet
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>

			<!-- Current characterSets -->
			<?php if(count($characterSets) > 0): ?>
				<div class="panel panel-default">
					<div class="panel-heading">
						Current characterSet
					</div>

					<div class="panel-body">
						<table class="table table-striped characterSet-table">
							<thead>
								<th>characterSet</th>
								<th>&nbsp;</th>
							</thead>
							<tbody>
								<?php foreach($characterSets as $characterSet): ?>
									<tr>
										<td class="table-text"><div><?php echo e($characterSet->characterSetName); ?></div></td>

										<!-- characterSet Delete Button -->
										<td>
											<form action="<?php echo e(url('manageCharacter/character')); ?>" method="GET">
												<input type="hidden" name="characterSet_id" value="<?php echo e($characterSet->id); ?>">
												<?php /*<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">*/ ?>
												<button type="submit" id="<?php echo e($characterSet->id); ?>" class="btn btn-danger">
													<i class="fa fa-btn fa-edit"></i>Edit
												</button>
											</form>
										</td>
										<td>
											<form action="<?php echo e(url('manageCharacterSet/delete-character-set/')); ?>" method="POST">
												<input type="hidden" name="characterSet_id" value="<?php echo e($characterSet->id); ?>">
												<?php /*<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">*/ ?>
												<button type="submit" id="<?php echo e($characterSet->id); ?>" class="btn btn-danger">
													<i class="fa fa-btn fa-trash"></i>Delete
												</button>
											</form>
										</td>
									</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>