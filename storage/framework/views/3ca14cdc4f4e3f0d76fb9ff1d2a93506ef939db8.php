<?php $__env->startSection('content'); ?>
	<div class="container">
		<div class="col-sm-offset-1 col-sm-10">
			<div class="panel panel-default">
				<div class="panel-heading">
					New Game
				</div>

				<div class="panel-body">
					<!-- Display Validation Errors -->
					<?php echo $__env->make('common.errors', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

					<!-- New game Form -->
					<form action="/manageGame/game" method="POST" class="form-horizontal">
						<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
						<!-- game Name -->
						<div class="form-group">
							<label for="game-gameName" class="col-sm-3 control-label">Game</label>

							<div class="col-sm-6">
								<input type="text" name="gameName" id="game-gameName" class="form-control">
							</div>
						</div>

						<div class="form-group">
							<label for="game-description" class="col-sm-3 control-label">Description</label>

							<div class="col-sm-6">
								<input type="text" name="description" id="game-description" class="form-control">
							</div>
						</div>

						<!-- Add game Button -->
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-6">
								<button type="submit" class="btn btn-default">
									<i class="fa fa-btn fa-plus"></i>Add Game
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>

			<!-- Current games -->
			<?php if(count($games) > 0): ?>
				<div class="panel panel-default">
					<div class="panel-heading">
						Current Game
					</div>

					<div class="panel-body">
						<table class="table table-striped game-table">
							<thead>
								<th>Game</th>
								<th>&nbsp;</th>
							</thead>
							<tbody>
								<?php foreach($games as $game): ?>
									<tr>
										<td class="table-text"><div><?php echo e($game->gameName); ?></div></td>

										<!-- gamelevel Button -->
										<td>
											<form action="<?php echo e(url('manageGameLevel/game-level')); ?>" method="GET">
												<input type="hidden" name="id" value="<?php echo e($game->id); ?>">
												<?php /*<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">*/ ?>
												<button type="submit" id="<?php echo e($game->id); ?>" class="btn btn-danger">
													<i class="fa fa-btn fa-edit"></i>Edit
												</button>
											</form>
										</td>

										<!-- game Delete Button -->
										<td>
											<form action="<?php echo e(url('manageGame/delete-game')); ?>" method="POST">
												<input type="hidden" name="id" value="<?php echo e($game->id); ?>">
												<?php /*<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">*/ ?>
												<button type="submit" id="<?php echo e($game->id); ?>" class="btn btn-danger">
													<i class="fa fa-btn fa-trash"></i>Delete
												</button>
											</form>
										</td>
									</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>