<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class User extends Authenticatable
{


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'self_into', 'photo_url', 'photo_path','gender','dateOfBirths','reg_code'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','created_at','updated_at'
    ];


    public function getLastUpdateTimeForHumans()
    {

        return $this->updated_at->diffForHumans();
    }


    public function getSmallProfileImage(){

        if($this->photo_url==null)
            return false;

        return url('user-image-small/'.$this->photo_url);

    }


    public function getMediumProfileImage(){

        if($this->photo_url==null)
            return false;

        return url('user-image-medium/'.$this->photo_url);

    }

    public function getProfileImage(){

        if($this->photo_url==null)
            return false;

        return url('user-image/'.$this->photo_url);

    }

    public function isAdmin(){

        if($this->is_admin==1)
            return true;

        return false;

    }

    public function getAccessableLevel(){
        

    }



    /**
     * Get all of the tasks for the user.
     */
    public function tasks()
    {
        return $this->hasMany(Task::class);
    }

    public function gameRecords()
    {
        return $this->hasMany(GameRecord::class);
    }


}
