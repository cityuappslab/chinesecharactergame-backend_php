<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Character extends Model
{
    //
    protected $table = 'character';


    public function characterset()
    {
        return $this->belongsTo('App\CharacterSet');
    }


    public function isCompleted()
    {


        if ($this->oneCharacter == '.')
            return false;
        // if ($this->image_url == 'null')
        //     return false;
        if ($this->audio_url == 'null')
            return false;


        return true;

    }

    public function getDetail(){
        $data['oneCharacter']=$this->oneCharacter;
        // $data['image_url']=url($this->image_url);
        $data['audio_url']=url($this->audio_url);

        return $data;
    }


}


