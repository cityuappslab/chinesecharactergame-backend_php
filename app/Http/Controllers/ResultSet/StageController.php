<?php

namespace App\Http\Controllers\ResultSet;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use App\ShowStage;
use DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class StageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->user = Auth::user();

        if ($this->user == null) {
            $data['error'] = true;
            $data['msg'] = 'Please login before request this page';
            return response()->json($data, 200);
        }

        $this->user_id = $this->user->id;


    }

    public function store(Request $request)
    {
    	$data['error'] = false;

    	$validator = Validator::make($request->all(),[
    		'stage' => 'required',
    		'shown' => 'required',
    		]);

    	$duplicateStageRecord = ShowStage::where('user_id',$this->user_id)->where('stage',$request->stage)->first();

    	if($validator->fails()){
    		$data['error'] = true;
    		$data['msg'] = $validator->errors()->all();
    	} else if($duplicateStageRecord != null){
    		$duplicateStageRecord->shown = $request->shown;
    		$duplicateStageRecord->save();

    	} else {
    		$stage = $request->stage;
    		$shown = $request->shown;

    		$showStage = new ShowStage();
    		$showStage->user_id = $this->user_id;
    		$showStage->stage = $stage;
    		$showStage->shown = $shown;

    		$showStage->save();
    	}
    	
    	return response()->json($data, 200);
    }
}
