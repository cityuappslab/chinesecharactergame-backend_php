<?php

namespace App\Http\Controllers\ResultSet;


use App\CharacterSet;
use App\Game;
use App\GameLevel;
use App\GameRecord;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class DataSetController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    //
    public function index(Request $request)
    {
        $users = User::paginate(4);


//        $data['pageCount']=$users->count();
//        $data['currentPage']=$users->currentPage();
//        $data['previousPageUrl']=$users->previousPageUrl();
//        $data['nextPageUrl']=$users->nextPageUrl();
//        $data['hasMorePages']=$users->hasMorePages();
//        $data['perPage']=$users->perPage();
        $data = $users->toArray();


        return view('record.users', $data);
        return response()->json($data, 200);

    }

    public function getUserRecord(Request $request)
    {
        if (!isset($request->id)) {
            return redirect('records/users');
        }
        $id = $request->id;

        $user = User::find($id);

        // $records = $user->gameRecords()->orderBy('gamerecord.created_at')->paginate(15)->toArray();

        $records = GameRecord::where('user_id', $id)->orderBy('created_at', 'desc')->paginate(15)->toArray();

        $numberOfRecords = $user->gameRecords()->count();

        $numberofPages = ceil($numberOfRecords/15);

        if ($records['next_page_url'] != null)
            $records['next_page_url'] = $records['next_page_url'] . '&id=' . $id;
        if ($records['prev_page_url'] != null)
            $records['prev_page_url'] = $records['prev_page_url'] . '&id=' . $id;

        $records['last_page_url'] = url('/').'/records/user?page='.$numberofPages.'&id='.$id ;

        $games = Game::all()->toArray();
        $names = [];
        foreach ($games as $game) {
            $names[$game['id']] = $game['gameName'];
        }

        $index = 0;
        foreach ($records['data'] as $record) {
            $records['data'][$index]['gameName'] = $names[$record['game_id']];
            $records['data'][$index++]['gameLevel'] = GameLevel::find([$record['gamelevel_id']])->first()->level;
        }


        $data = $records;
        $data['user'] = $user;
        $data['numberofPages'] = $numberofPages;

        return view('record.user', $data);
        return response()->json($data, 200);

    }

    public function getRecord($id=null)
    {
        if($id==null)
            return redirect()->back();

        $record=GameRecord::find($id);

        $recordDetail=DB::table('trialrecord')->where('gamerecord_id', $record->id)->get();

        $user = User::find($record['user_id']);

        $gameLevel = GameLevel::find($record->gamelevel_id);
        $game = Game::find($record->game_id);

        $data['record']=$record->toArray();
        $data['trials']=$recordDetail;
        $data['user']=$user;
        $data['game']=$game->toArray();
        $data['gamelevel']=$gameLevel->toArray();
//        $data['characterSetName']=CharacterSet::find($gameLevel->characterSet_id)->characterSetName;

//        dd($data);

        return view('record.record', $data);
        return response()->json($data, 200);

    }


    public function getGameRecord(Request $request)
    {
        if (!isset($request->id)) {
            return redirect('records/');
        }

        $id = $request->id; // 8, 9, 10

        $game = Game::find($id);

        $records =  GameRecord::where('game_id', $id)->orderBy('created_at', 'desc')->paginate(15)->toArray();

        $numberOfRecords = $game->gameRecords()->count();

        $numberofPages = ceil($numberOfRecords/15);

        if ($records['next_page_url'] != null)
            $records['next_page_url'] = $records['next_page_url'] . '&id=' . $id;
        if ($records['prev_page_url'] != null)
            $records['prev_page_url'] = $records['prev_page_url'] . '&id=' . $id;

        $records['last_page_url'] = url('/').'/records/game?page='.$numberofPages.'&id='.$id ;
        
        $games = Game::all()->toArray();

        $names = [];
        foreach ($games as $game) {
            $names[$game['id']] = $game['gameName'];
        }



        $index = 0;
        foreach ($records['data'] as $record) {
            $records['data'][$index]['gameName'] = $names[$record['game_id']];
            $records['data'][$index]['gameLevel'] = GameLevel::find([$record['gamelevel_id']])->first()->level;
            $records['data'][$index++]['userName']=(User::find($record['user_id'])->name);
            // index++ supply user info to each data
        }



        $game = Game::find($id);


        $data = $records;
        $data['game'] = $game;

//        dd($data);


        return view('record.game', $data);
        return response()->json($data, 200);

    }


}
