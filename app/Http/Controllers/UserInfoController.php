<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\UserInfoRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Validator;
use Storage;
use Symfony\Component\HttpFoundation\Request;

class UserInfoController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
        $this->user = Auth::user();
    }

    public function getUpdate()
    {


        return view('auth.userinfo', ['user' => $this->user]);

    }


    public function postUpdate(UserInfoRequest $request)
    {

        $updateInfo = [
            'name' => $request->name,
            'self_into' => $request->self_into,

        ];

        $result = $this->user->update($updateInfo);


        //Uploading Projfile--------------------------------------start
        if (Input::hasFile('profile_photo')) {
            $file = $request->file('profile_photo');
            $ext = $file->getClientOriginalExtension();

            $imageName = rand(0, 100000);
            $imageNameWithExtension = $imageName . '.' . 'png';

            $iterator = 0;
            do {
                $exists = Storage::disk('local')->exists('/userProfile/' . $imageNameWithExtension);
                $iterator++;
                if ($iterator > 20) {
                    Log::debug('id:' . $this->user->id . ' Name:' . $this->user->name . 'profile photo unable to upload in app/Http/Controller/Auth/UserinfoController@postUpdate');
                    return redirect('userinfo/update');
                }
            } while ($exists);
            Image::make($request->file('profile_photo'))->encode('png', 75)->save('../storage/app/userProfile/' . $imageNameWithExtension);


            $this->user->photo_url = $imageName;
            $this->user->photo_path = '../storage/app/userProfile/' . $imageNameWithExtension;
            $this->user->save();
        }
        //Uploading Projfile--------------------------------------end


        //if update successful
        if ($result == true) {
            return view('auth.userinfo',
                [
                    'user' => $this->user,
                    'result' => 'Update Successful'
                ]
            );
        }

        //else alert the erro message
        return view('auth.userinfo', ['user' => $this->user]);
    }


    public function postUpdateInfo(Request $request)
    {

        $data['error'] = false;


        // $validator = Validator::make($request->all(),
        //     [
        //         'gender' => 'required' ,
        //         'dateOfBirths' => 'required|date_format:"Y-m-d"',
        //         'educationLevel' => 'required'
        //     ]
        // );

//        dd($validator->errors());
//        dd($request->gender);

        if ($validator->fails()) {
            $data['error'] = true;
            $data['msg'] = $validator->errors()->all();
        } else {

            if($request->name!=null)
                $this->user->name=$request->name;
            if($request->self_into!=null)
                $this->user->self_into=$request->self_into;

            $result = $this->user->update();

            if ($result == true) {

                $user =$this->user;
                $data['user']['name']=$user['name'];
                $data['user']['email']=$user['email'];
//            $data['user']['photo_url']=$user['photo_url'];


                $data['msg'] ='Update Successful';

            }
        }

        return response()->json($data, 200);

    }


}
