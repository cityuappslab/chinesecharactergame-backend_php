<?php

namespace App\Http\Controllers\Admin;

use App\Game;
use App\GameLevel;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\GameRequest;
use Illuminate\Http\Request;


class GameController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');

        $this->middleware('admin');


    }


    public function getGame()
    {
        $games=Game::all();

        return view('game.index',
            [
                'games'=>$games

            ]);

    }

    public function postGame(GameRequest $request)
    {
        $gameName=$request->gameName;
        $description=$request->description;

        $game=new Game();

        $game->gameName=$gameName;
        $game->description=$description;
        $game->save();

        $game_id=Game::where('gameName',$gameName)->orderBy('created_at', 'desc')->first()->id;


        for($i=1;$i<=60;$i++)
        {
            $gameLevel=new GameLevel();
            $gameLevel->game_id=$game_id;
            $gameLevel->characterSet_id='null';
            $gameLevel->level=$i;
            $gameLevel->duration=61-$i;
            $gameLevel->save();
        }



        return redirect('manageGame/game');

    }


    public function postDeleteGame(Request $request)
    {
        $game_id=$request->id;

        $game=Game::where('id',$game_id);

        $game->delete();

        return redirect('manageGame/game');

    }


}
