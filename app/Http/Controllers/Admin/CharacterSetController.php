<?php

namespace App\Http\Controllers\Admin;

use App\Character;
use App\CharacterSet;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\CharacterSetRequest;
use Illuminate\Http\Request;

class CharacterSetController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');

        $this->middleware('admin');

    }


    public function getCharacterSet()
    {
        $characterSets = CharacterSet::all();



        return view('characterset.index',
            [
                'characterSets' => $characterSets
            ]);
    }


    public function postCharacterSet(CharacterSetRequest $request)
    {
        $minNumOfCharacter=15;

        $characterSetName = $request->characterSetName;
        $numOfCharacter = $minNumOfCharacter;

        $characterSet = new CharacterSet();
        $characterSet->characterSetName = $characterSetName;
        $characterSet->numOfCharacter = $numOfCharacter;
        $characterSet->save();

        $characterSet_id = CharacterSet::where('characterSetName', $characterSetName)->orderBy('created_at', 'desc')->first()->id;



        for ($i = 1; $i <= $numOfCharacter; $i++) {
            $character = new Character();
            $character->characterSet_id = $characterSet_id;
            $character->oneCharacter= '.';
            $character->audio_url = 'null';
            $character->save();
        }


        return redirect('manageCharacterSet/character-set');

    }


    public function postDeleteCharacterSet(Request $request)
    {
        $characterSet_id = $request->characterSet_id;

        $characterSet = CharacterSet::where('id', $characterSet_id);

        $characterSet->delete();

        return redirect('manageCharacterSet/character-set');

    }


}
